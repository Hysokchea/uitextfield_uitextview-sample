//
//  UIViewController.swift
//  WABOOKS_iOS_V2
//
//  Created by Mok Monita on 5/1/21.
//

import UIKit
import Photos

extension UIViewController {
      
    func gotoAppSettings() {
        if #available(iOS 10.0, *) {
            DispatchQueue.main.async {
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
            }
        }
    }
    
    func gotoPasscodeSettings() {
        if #available(iOS 10.0, *) {
            DispatchQueue.main.async {
                UIApplication.shared.open(URL(string: "App-Prefs:root=TOUCHID_PASSCODE" + Bundle.main.bundleIdentifier!)!, options: [:], completionHandler: nil)
            }
        }
    }
    
    func checkAllowNotificationPermission(completion: @escaping Completion_Bool) {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                if settings.authorizationStatus != .authorized {
                    DispatchQueue.main.async {
                        completion(false)
                    }
                }
                else {
                    DispatchQueue.main.async {
                        completion(true)
                    }
                }
            }
        }
    }
    
    func popOrDismissVC(animated: Bool = true, completion: @escaping Completion = { }) {
        if let nav = self.navigationController {
            nav.popViewController(animated: animated)
            completion()
        }
        else {
            self.dismiss(animated: animated) {
                completion()
            }
        }
    }
    
    
    func pushVC(viewController: UIViewController, animated: Bool = true) {
        self.navigationController?.pushViewController(viewController, animated: animated)
    }
    
    func navController(sbName: String, identifier: String) -> UINavigationController {
        return UIStoryboard(name: sbName, bundle: nil).instantiateViewController(withIdentifier: identifier) as! UINavigationController
    }
    
    // Important
    func setRootViewController(sbName: String, identifier: String, direction: UIWindow.TransitionOptions.Direction = .fade, duration: TimeInterval = 0.3) {
        let window              = UIApplication.shared.keyWindow
        var animateOption       = UIWindow.TransitionOptions()
        animateOption.direction = direction
        animateOption.duration  = duration
        window?.setRootViewController(VC(sbName: sbName, identifier: identifier), options: animateOption)
    }
    
    // Important
    func setRootViewController(viewController: UIViewController, direction: UIWindow.TransitionOptions.Direction = .fade, duration: TimeInterval = 0.3) {
        let window              = UIApplication.shared.keyWindow
        var animateOption       = UIWindow.TransitionOptions()
        animateOption.direction = direction
        animateOption.duration  = duration
        window?.setRootViewController(viewController, options: animateOption)
    }
    
    // Important
    func setRootNavViewController(sbName: String, identifier: String, direction: UIWindow.TransitionOptions.Direction = .fade, duration: TimeInterval = 0.3) {
        let window              = UIApplication.shared.keyWindow
        var animateOption       = UIWindow.TransitionOptions()
        animateOption.direction = direction
        animateOption.duration  = duration
        window?.setRootViewController(navController(sbName: sbName, identifier: identifier), options: animateOption)
    }
    
    // Important
    func setRootNavViewController(navigation: UINavigationController, direction: UIWindow.TransitionOptions.Direction = .fade, duration: TimeInterval = 0.3) {
        let window              = UIApplication.shared.keyWindow
        var animateOption       = UIWindow.TransitionOptions()
        animateOption.direction = direction
        animateOption.duration  = duration
        window?.setRootViewController(navigation, options: animateOption)
    }
    
    func getCGSizeText(st: String, fontSize: CGFloat) -> CGSize {
        let font = UIFont.systemFont(ofSize: fontSize)
        let fontAtrr = [NSAttributedString.Key.font: font]
        let size = (st as NSString).size(withAttributes: fontAtrr)
        return size
    }
    
    /*
     + TODO: Set Left Navigation Item
     - param image : Your image name
     - param action: Selector handle selection when click on left navigation
     - param title : Your title when navigation title is not available
     */
    func setLeftNavigationItem(_ image:String, action:Selector, title:String, width: CGFloat) -> Void {
        
        // Image
        let image_nav = UIImage(named: image)
        
        // Button
        let leftNavBtn = UIButton()
        leftNavBtn.setImage(image_nav, for: UIControl.State())
        leftNavBtn.setTitle(title, for: UIControl.State())
        leftNavBtn.titleLabel!.font = UIFont.systemFont(ofSize: 17)
        let autoWidth = getCGSizeText(st: title, fontSize: 17).width
        let frameBtn = CGRect(x: -16,y: 0,width: image_nav!.size.width + autoWidth, height: image_nav!.size.height)
        leftNavBtn.frame = frameBtn
        leftNavBtn.titleEdgeInsets = UIEdgeInsets(top: 0, left: -26, bottom: 0, right: 0)
        leftNavBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        leftNavBtn.contentHorizontalAlignment = .left
        leftNavBtn.addTarget(self, action: action, for: UIControl.Event.touchUpInside)
        
        // leftBarButtonItem
        let leftBarButtonItem = UIBarButtonItem(customView: leftNavBtn)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }
    
    func VC(sbName: String, identifier: String) -> UIViewController {
        return UIStoryboard(name: sbName, bundle: nil).instantiateViewController(withIdentifier: identifier)
    }
    
    func PopupVC(storyboard: String, identifier: String) -> UIViewController {
        let vc = UIStoryboard(name: storyboard, bundle: nil).instantiateViewController(withIdentifier: identifier)
        vc.modalPresentationStyle = .custom
        vc.modalTransitionStyle = .crossDissolve
        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        
        return vc
    }
  
    func pushViewController(sbName: String, identifier: String, animated: Bool = true)  {
        let vc = UIStoryboard(name: sbName, bundle: nil).instantiateViewController(withIdentifier: identifier)
        self.navigationController?.pushViewController(vc, animated: animated)
    }
     
    func presetVCWithNavigation(vc: UIViewController, modalPresentStyle: UIModalPresentationStyle = .fullScreen) {
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = modalPresentStyle
        self.present(nav, animated: true, completion: nil)
    }
    
    func presetVC(vc: UIViewController, modalPresentStyle: UIModalPresentationStyle = .fullScreen) {
        vc.modalPresentationStyle = modalPresentStyle
        self.present(vc, animated: true, completion: nil)
    }
     
    func alert(title: String, message: String, completion: @escaping Completion = { }) {
        if let jsonData = message.data(using: String.Encoding.utf8) {
            do {
                try JSONSerialization.jsonObject(with: jsonData)
                alertJsonError(title: title, message: message)
            }
            catch {
                let alert   = UIAlertController(title: title, message: message, preferredStyle: .alert)
                let ok      = UIAlertAction(title: "OK".capitalized, style: .cancel) { (_) in
                    completion()
                }
                alert.addAction(ok)
                
                present(alert, animated: true, completion: nil)
            }
        }
    }
    
    private func alertJsonError(title: String, message: String) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.left
        
        let messageText = NSMutableAttributedString(
            string: message,
            attributes: [
                NSAttributedString.Key.paragraphStyle: paragraphStyle,
                NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12),
                NSAttributedString.Key.foregroundColor : UIColor.black
            ]
        )
        
        let alert   = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok      = UIAlertAction(title: "OK".capitalized, style: .cancel, handler: nil)
        alert.addAction(ok)
        alert.setValue(messageText, forKey: "attributedMessage")
        
        present(alert, animated: true, completion: nil)
    }
    
    func alertYesNo(title: String, message: String, nobtn: String = "NO".capitalized, yesbtn: String = "YES".capitalized
                    , completion: @escaping Completion_Bool) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(
            UIAlertAction(title: yesbtn, style: .default, handler: { (action) -> Void in
                completion(true)
            })
        )
        alert.addAction(
            UIAlertAction(title: nobtn, style: .cancel)
        )
        
        self.present(alert, animated: true, completion: nil)
    }
      
    func alert(title: String = "Notice", message: String, okbtn: String = "Confirm", completion: @escaping Completion = { }) {
        if let jsonData = message.data(using: String.Encoding.utf8) {
            do {
                try JSONSerialization.jsonObject(with: jsonData)
                alertJsonError(title: title, message: message)
            }
            catch {
                let alert   = UIAlertController(title: title, message: message, preferredStyle: .alert)
                let ok      = UIAlertAction(title: okbtn, style: .cancel) { (_) in
                    completion()
                }
                alert.addAction(ok)
                
                present(alert, animated: true, completion: nil)
            }
        }
    }
      
    func checkAllowPhotoPermission(completion: @escaping Completion_Bool) {
        if (PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.authorized) {
            DispatchQueue.main.async {
                completion(true)
            }
        }
        else if (PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.notDetermined) {
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                if (newStatus == PHAuthorizationStatus.authorized) {
                    DispatchQueue.main.async {
                        completion(true)
                    }
                }
                else {
                    DispatchQueue.main.async {
                        completion(false)
                    }
                }
            })
        }
        else {
            DispatchQueue.main.async {
                completion(false)
            }
        }
    }
    
    func checkAllowCameraPermission(completion: @escaping Completion_Bool) {
        if AVCaptureDevice.authorizationStatus(for: .video) != .authorized {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) -> Void in
                DispatchQueue.main.async {
                    completion(granted)
                }
            })
        }
        else {
            DispatchQueue.main.async {
                completion(true)
            }
        }
    }
    
    func openCamera(cameraBlock:(()->Void)?) {
        if(UIImagePickerController.isSourceTypeAvailable(.camera))
        {
            self.checkAllowCameraPermission { (status) in
                if status {
                    cameraBlock?()
                }
            }
            
        }
    }
    
    func openAlbum(albumBlock:(()->Void)?) {
        self.checkAllowPhotoPermission { (status) in
            if status {
                albumBlock?()
            }
        }
    }
    
    func checkDeniedCameraPermission(completion: @escaping Completion_Bool) {
        if AVCaptureDevice.authorizationStatus(for: .video) == .denied {
            DispatchQueue.main.async {
                completion(true)
            }
        }
        else {
            DispatchQueue.main.async {
                completion(false)
            }
        }
    }
     
    func checkDeniedPhotoPermission(completion: @escaping Completion_Bool) {
        if (PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.denied) {
            DispatchQueue.main.async {
                completion(true)
            }
        }
        else {
            DispatchQueue.main.async {
                completion(false)
            }
        }
    }
       
}

extension UIViewController {
    func callCommonPopup(withStorybordName storyboard: String, identifier: String) -> UIViewController {
        let vc = UIStoryboard(name: storyboard, bundle: nil).instantiateViewController(withIdentifier: identifier)
        vc.modalPresentationStyle                       = .overFullScreen
        vc.modalTransitionStyle                         = .crossDissolve
        vc.providesPresentationContextTransitionStyle   = true
        vc.definesPresentationContext                   = true
        return vc
    }
    
    func showToast(image: String, message: String) {
        //create view
        let myView = UIView()
        myView.frame = CGRect(x: 20,
                              y: -65 ,
                              width: self.view.bounds.width - 40, height: 54)
        myView.cornerRadius = 5
        myView.backgroundColor = UIColor(hexString: "49535B")
        
        //create image
        let imageView = UIImageView(frame: CGRect(x: 15, y: myView.bounds.midY - (24 / 2), width: 24, height: 24))
        imageView.image = UIImage(named: image)
        myView.addSubview(imageView)
        
        //create label
        let toastLabel = UILabel(frame: CGRect(x: 49, y: myView.bounds.midY - (myView.bounds.height / 2), width: myView.bounds.width, height: myView.bounds.height))
        toastLabel.textColor = .white
        toastLabel.textAlignment = .left
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.clipsToBounds  =  true
        toastLabel.numberOfLines = 0
        
        myView.addSubview(toastLabel)
        self.view.addSubview(myView)
        
        // Finally the animation!
        let offset = CGPoint(x: 0, y: -self.view.frame.maxY)
        let x: CGFloat = 0, y: CGFloat = 0
        myView.transform = CGAffineTransform(translationX: offset.x + x, y: offset.y + y + 500)
        myView.isHidden = false
        
        //view come in
        UIView.animate(
            withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.80, initialSpringVelocity: 3,
            options: .curveEaseOut, animations: {
                myView.transform = .identity
                myView.alpha = 1
            })
        
        //view come out
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            UIView.animate(
                withDuration: 5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 3,
                options: .curveEaseIn, animations: {
                    myView.center.y -= myView.bounds.height + 100
                },  completion: {(_ completed: Bool) -> Void in
                    myView.removeFromSuperview()
                })
        }
    }
     
}
