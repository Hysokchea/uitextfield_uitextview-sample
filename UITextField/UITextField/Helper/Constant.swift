//
//  Constant.swift
//  UITextField
//
//  Created by Bun on 9/1/23.
//

import Foundation

enum NotifyKey : String {
    case updateUsage                        = "updateUsage"
    case reloadMainVC                       = "reloadMainVC"
    case reloadAfterUploadImage             = "reloadAfterUploadImage"
    case reloadAfterUpdateOrDeleteOrSubmit  = "reloadAfterUpdateOrDeleteOrSubmit"
    case reloadLocalize                     = "reloadLocalize"
    case reloadSelectedPurpose              = "reloadSelectedPurpose"
    case reloadNotificationVC               = "reloadNotificationVC"
    case reloadReceiptListVC                = "reloadReceiptListVC"
    case reloadReceiptListWebView           = "reloadReceiptListWebView"
    
    case gotoSubmitReceiptVC                = "gotoSubmitReceiptVC"
    case gotoReceiptDetailVC                = "gotoReceiptDetailVC"
    case gotoPhotoPreviewVC                 = "gotoPhotoPreviewVC"
    case gotoNotification                   = "gotoNotification"
    case checkingBadgeCount                 = "checkingBadgeCount"
    case presentLimitedLibraryPicker        = "presentLimitedLibraryPicker"
    case imageCount                         = "imageCount"
    case SuccessTakePhoto                   = "SuccessTakePhoto"
    case SuccessTakePhotoFromSubmitReceiptVC = "SuccessTakePhotoFromSubmitReceiptVC"
}

enum UserDefaultKey : String {
    
    case isHiddenAutoLayout         = "_UIConstraintBasedLayoutLogUnsatisfiable"
    case appLang                    = "appLang"
    case autoLogin                  = "autoLogin"
    case IS_FIRST_COACH_APP         = "IS_FIRST_COACH_APP"
    case deviceToken                = "DeviceToken"
    case firstTimeRunApp            = "firstTimeRunApp"
    case hasReceivedNotification    = "hasReceivedNotification"
    case wabooksVersion             = "wabooksVersion"
    case isShowSelectVersionApp     = "isShowSelectVersionApp"
}

enum CustomOptionCellType : String, CaseIterable {
    case Detail         = "Detail"
    case MultipleChoice = "MultipleChoice"
}

typealias Completion                = ()                -> Void
typealias Completion_Int            = (Int)             -> Void
typealias Completion_Bool           = (Bool)            -> Void
typealias Completion_NSError        = (NSError?)        -> Void
typealias Completion_String         = (String)          -> Void
typealias Completion_String_Error   = (String, Error?)  -> Void
