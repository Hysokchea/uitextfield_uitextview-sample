//
//  Share.swift
//  WABOOKS_iOS_V2
//
//  Created by Mok Monita on 5/1/21.
//

import Foundation

enum LanguageCode: String {
    case Korean     = "ko"
    case English    = "en"
    case Khmer      = "km"
    case Vietnamese = "vi"
}

enum WabooksVersion: String {
    case Cambodia   = "Cambodia"
    case Vietnam    = "Vietnam"
}
 
struct Shared {
    
    //MARK:- singleton
    static var share = Shared()
    
    private init() { }
    
    static var wabooksVersion           : WabooksVersion    = .Cambodia
    static var language                 : LanguageCode      = .Khmer
    static var isDevelopServer          = false
    var token                           : String?
    
    static var API_SEQ_NO               = ""
    static var LST_TRAN_YN              = ""
    static var deviceToken              = ""
    static var selectUserListKey        = ""
    
    static var strDuration              = ""
    
    static var user_id                  = ""
    
    //Use in CustomPopUpCell
    static var indexPath                = 0
    
    //Use in CustomOptionPopup
    static var indexPathOption          = 0
    
    //Use in CustomOptionPopup
    static var indexPathDetail          = 0
    
    //Use in Select Country
    static var indexPathCountry         = 0
    
    static var badgeCount               = 0
    static var hasReceivedNewNotification = false
    static var isFromMainTabBar         = false
    
    static var c_base_url            = ""
    static var c_base_url_kh         = ""
    static var c_base_url_vn         = ""
    static var c_web_base_url        = ""
    static var selectReceiptRowAt       : Int = -1
    static var selectReceiptSectionAt   : Int = -1
    
    static let MAX_IMAGES           : Int = 5
    static var CURRENT_MAX_IMAGES   : Int = 5
     
    func getCurrentDate(date: Date) -> (dateString: String, serverDate: String, serverTime: String) {
        var calendar    = Calendar.current
        calendar.locale = Locale(identifier: Shared.language.rawValue)
        let component   = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date)
        
        let month   = String(format: "%02d", component.month!)
        let day     = String(format: "%02d", component.day!)
        let hour    = String(format: "%02d", component.hour!)
        let minute  = String(format: "%02d", component.minute!)
        let second  = String(format: "%02d", component.second!)
        
        let dateString      = "\(day)-\(month)-\(component.year!)"
        let serverString    = "\(component.year!)\(month)\(day)"
        let serverTime      = "\(hour)\(minute)\(second)"
        
        return (dateString, serverString, serverTime)
    }
     
    func getLocalizedFont(preFontName: String) -> String {
        
        var fontName = "notoserifkhmer-semicondensed";
        
        if Shared.language.rawValue == LanguageCode.English.rawValue {
            fontName = "inter";
        } else if Shared.language == .Korean {
            fontName = "nanumsquare"
        }
        
        if Shared.language.rawValue == LanguageCode.English.rawValue { //English
            
            if preFontName.range(of: "notoserifkhmer-semicondensed") != nil {//Khmer
                if preFontName.range(of: "bold") != nil {
                    fontName += "-bold"
                }
                else if preFontName.range(of: "medium") != nil {
                    fontName += "-medium"
                } else {
                    fontName += "-regular"
                }
            } else if preFontName.range(of: "nanumsquare") != nil { //Korean
                if preFontName.range(of: "eb") != nil { //Bold
                    fontName += "-bold"
                } else if preFontName.range(of: "b") != nil {
                    fontName += "-medium"
                } else {
                    fontName += "-regular"
                }
            } else if preFontName.range(of: "nanumsquare") != nil { //vietnamese
                if preFontName.range(of: "eb") != nil { //Bold
                    fontName += "-bold"
                } else if preFontName.range(of: "b") != nil {
                    fontName += "-medium"
                } else {
                    fontName += "-regular"
                }
            }else {
                fontName = preFontName
            }
            
        } else if Shared.language == .Khmer { //Khmer
            
            if preFontName.range(of: "inter") != nil {//English
                if preFontName.range(of: "bold") != nil {
                    fontName += "bold"
                } else if preFontName.range(of: "medium") != nil {
                    fontName += "medium"
                } else if preFontName.range(of: "black") != nil {
                    fontName = "inter-black"
                } else {
                    fontName += ""
                }
            }
            else if preFontName.range(of: "nanumsquare") != nil {//Korean
                if preFontName.range(of: "eb") != nil { //Bold
                    fontName += "bold"
                } else if preFontName.range(of: "b") != nil {
                    fontName += "medium"
                } else {
                    fontName += ""
                }
            } else if preFontName.range(of: "nanumsquare") != nil {//vietnamese
                if preFontName.range(of: "eb") != nil { //Bold
                    fontName += "bold"
                } else if preFontName.range(of: "b") != nil {
                    fontName += "medium"
                } else {
                    fontName += ""
                }
            }else {
                fontName = preFontName
            }
            
        } else if Shared.language == .Korean { //Korean
            
            //English
            if preFontName.range(of: "inter") != nil {//English
                if preFontName.range(of: "bold") != nil {
                    fontName += "eb"
                } else if preFontName.range(of: "medium") != nil {
                    fontName += "b"
                } else if preFontName.range(of: "black") != nil {
                    fontName = "inter-black"
                } else {
                    fontName += "r"
                }
            }
            else if preFontName.range(of: "notoserifkhmer-semicondensed") != nil {//Khmer
                if preFontName.range(of: "bold") != nil {
                    fontName += "eb"
                }
                else if preFontName.range(of: "medium") != nil {
                    fontName += "b"
                } else {
                    fontName += "r"
                }
            }
            else if preFontName.range(of: "notoserifkhmer-semicondensed") != nil {//vietnamese
                if preFontName.range(of: "bold") != nil {
                    fontName += "eb"
                }
                else if preFontName.range(of: "medium") != nil {
                    fontName += "b"
                } else {
                    fontName += "r"
                }
            }
            
            else {
                fontName = preFontName
            }
            
            
        }else if Shared.language == .Vietnamese {
            
            if preFontName.range(of: "inter") != nil {//English
                if preFontName.range(of: "bold") != nil {
                    fontName += "bold"
                } else if preFontName.range(of: "medium") != nil {
                    fontName += "medium"
                } else if preFontName.range(of: "black") != nil {
                    fontName = "inter-black"
                } else {
                    fontName += ""
                }
            }
            else if preFontName.range(of: "nanumsquare") != nil {//Korean
                if preFontName.range(of: "eb") != nil { //Bold
                    fontName += "bold"
                } else if preFontName.range(of: "b") != nil {
                    fontName += "medium"
                } else {
                    fontName += ""
                }
                
            }
            else if preFontName.range(of: "notoserifkhmer-semicondensed") != nil {//Khmer
                if preFontName.range(of: "bold") != nil {
                    fontName += "eb"
                }
                else if preFontName.range(of: "medium") != nil {
                    fontName += "b"
                } else {
                    fontName += "r"
                }
            }
        }
        
        return fontName
    }
    
    
//    func getLocalizedFont(preFontName: String) -> String {
//
//        var fontName = "Battambang";
//        if Shared.language.rawValue == LanguageCode.English.rawValue {
//            fontName = "Rubik";
//        }
//
//        if Shared.language.rawValue == LanguageCode.English.rawValue {
//
//            if preFontName.range(of: "bold") != nil {
//                fontName += "-Bold";
//            }
//            else if preFontName.range(of: "medium") != nil {
//                fontName += "-Medium";
//            }
//            else if preFontName.range(of: "regular") != nil {
//
//                // In case of changing language | Label is still Battambang-regular font name
//                if preFontName.range(of: "Battambang") != nil {
//                    fontName += "-Medium";
//                }
//                else {
//                    fontName += "-Regular";
//                }
//
//            }
//            else { // Light
//                fontName += "-Regular";
//            }
//
//        } else {
//
//            if preFontName.range(of: "bold") != nil {
//                fontName += "-Bold";
//            }
//            else if preFontName.range(of: "regular") != nil {
//
//                // In case of changing language | Label is still rubik-medium font name
//                if preFontName.range(of: "rubik") != nil {
//                    fontName += "-Regular";
//                }
//                else {
//                    fontName += "-Regular"; // Light if design doesn't agree find another solution
//                }
//            }
//            else if preFontName.range(of: "medium") != nil {
//
//                // In case of changing language | Label is still rubik-medium font name
//                if preFontName.range(of: "rubik") != nil {
//                    fontName += "-Regular";
//                }
//                else {
//                    fontName += "-Regular";
//                }
//            }
//            else { // Regular or Light
//                fontName += "-Regular"; // Light if design doesn't agree find another solution
//            }
//
//        }
//
//        return fontName
//    }
}
