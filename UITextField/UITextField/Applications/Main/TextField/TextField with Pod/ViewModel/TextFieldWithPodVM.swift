//
//  NormalVM.swift
//  UITextFieldAndUITextView
//

import Foundation

class TextFieldWithPodVM {
     
    var withPodRec : [TextFieldWithPodModel.WithPodObject]?
    
    func initTextFieldWithPod() {
        self.withPodRec = [
            TextFieldWithPodModel.WithPodObject(rowType         : .Username,
                                                title           : "Username",
                                                value           : "",
                                                placeholder     : "Enter username",
                                                helperText      : "Username must be 4-15 characters",
                                                isValidation    : false),
            
            TextFieldWithPodModel.WithPodObject(rowType         : .Email,
                                                title           : "Email",
                                                value           : "",
                                                placeholder     : "Enter email",
                                                helperText      : "Email is invalid",
                                                isValidation    : false),
            
            TextFieldWithPodModel.WithPodObject(rowType         : .Password,
                                                title           : "Password",
                                                value           : "",
                                                placeholder     : "Enter password",
                                                helperText      : "Password is invalid",
                                                isValidation    : false),
            
            TextFieldWithPodModel.WithPodObject(rowType         : .ConfirmPassword,
                                                title           : "Confirm Password",
                                                value           : "",
                                                placeholder     : "Enter confirm password",
                                                helperText      : "Confirm password is invalid",
                                                isValidation    : false)
        ]
    }
    
   /// Set value based on rowType
   func setValue(rowType: RowType, strValue: String, isValidation: Bool) {
       for (index, value) in (self.withPodRec?.enumerated())! {
           if value.rowType == rowType {
               self.withPodRec?[index].value           = strValue
               self.withPodRec?[index].isValidation    = isValidation
           }
       }
   }
   
   /// Get value based on rowType
   func getValue(rowType: RowType) -> String {
       var strValue = ""
       for (index, value) in (self.withPodRec?.enumerated())! {
           if value.rowType == rowType {
               strValue = self.withPodRec?[index].value ?? ""
           }
       }
       return strValue
   }
   
   /// Get row validation based on rowType
   func getIsValidation(rowType: RowType) -> Bool {
       var isValidation = false
       for (index, value) in (self.withPodRec?.enumerated())! {
           if value.rowType == rowType {
               isValidation = self.withPodRec?[index].isValidation ?? false
           }
       }
       return isValidation
   }
    
}
