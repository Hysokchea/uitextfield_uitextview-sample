//
//  TextFieldWithPodCell.swift
//  UITextField
//
//  Created by Hong Kheatbung on 8/1/23.
//

import UIKit

import MaterialComponents

class TextFieldWithPodCell: UITableViewCell {

    @IBOutlet weak var textField    : MDCOutlinedTextField!
    
    let passwordButton              = UIButton(type: .custom)
       
    var textFieldWithPodVM          = TextFieldWithPodVM()
    var textFieldWithPodDataRec     : TextFieldWithPodModel.WithPodObject?
    var completionRec               : () -> Void = {}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.textField.delegate = self  // Delegate textField
        self.textField.setOutlineColor(.systemGray6, for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    @objc func didTapBtnEye(_ sender: UIButton) {
        self.passwordButton.isSelected      = !sender.isSelected
        self.textField.isSecureTextEntry    = !sender.isSelected
    }
    
    // Config data to display
    func configCell(data: TextFieldWithPodModel.WithPodObject?) {
        self.textField.placeholder                  = data?.placeholder
        self.textField.borderColor                  = .none
        self.textField.borderWidth                  = 0
        self.textField.preferredContainerHeight     = 50
        self.textField.setFloatingLabelColor(UIColor(hexString: "A6A6A6"), for: .normal)
        self.textField.sizeToFit()
        
        let rowType = data?.rowType
        
        switch rowType {
        case .Password, .ConfirmPassword:
            let imageOpenEye        = UIImage(systemName:"eye.slash.fill")
            let imageCloseEye       = UIImage(systemName:"eye.fill")
            passwordButton.frame    = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(39), height: CGFloat(30))
            passwordButton.setImage(imageOpenEye, for: .normal)
            passwordButton.setImage(imageCloseEye, for: .selected)
            passwordButton.addTarget(self, action: #selector(didTapBtnEye(_:)), for: .touchUpInside)
            
            self.textField.isSecureTextEntry    = true
            self.textField.trailingView         =  passwordButton
            self.textField.trailingViewMode     = .always
            
        default:
            self.textField.trailingViewMode = .never
        }
    }
    
    // Emial validation
    func isValidEmail(email: String) -> Bool {
        let emailRegEx  = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred   = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    // Limit length for username from 4-15
    func isValidUsername(username: String) -> Bool {
        let RegEx       = "\\A\\w{4,15}\\z"
        let Test        = NSPredicate(format:"SELF MATCHES %@", RegEx)
        return Test.evaluate(with: username)
    }
    
    // Password validation
    func isValidPassword(password: String) -> Bool {
        let passwordRegex   = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*()\\-_=+{}|?>.<,:;~`’]{8,}$"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: password)
    }
    
    // Comfirm password validation
    func isValidConfirmPassword(password: String, confirmPassword: String) -> Bool {
        // Checked: confirm password and password are matched
        let isPasswordMatch = password == confirmPassword
        let passwordRegex   = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*()\\-_=+{}|?>.<,:;~`’]{8,}$"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: confirmPassword) && isPasswordMatch
    }
}
 
// MARK: - UITextFieldDelegate
extension TextFieldWithPodCell: UITextFieldDelegate {
    //Dismiss keyboard when user's click Return or Done key
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        // Set borderColor and borderWidth for view that contain textField
        self.textField.label.text   = textFieldWithPodDataRec?.title
        self.textField.setOutlineColor(.blue, for: .editing)
        
        let rowType = textFieldWithPodDataRec?.rowType
        
        switch rowType {
            
        case .Email:
            // Set keyboard to email keyboard
            textField.keyboardType = .emailAddress
            
        // Default for Username
        default:
            // Set keyboard to default
            textField.keyboardType = .default
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        // Trimming WhiteSpace (" ")
        let updatedText = textField.text?.trimmingCharacters(in: .whitespaces) ?? ""
        let rowType = textFieldWithPodDataRec?.rowType
        switch rowType {
        case .Email:
            // Checked textField has value or not
            // If Yes : Checked email validation
            let isValidation = self.isValidEmail(email: updatedText)
            self.textFieldWithPodVM.setValue(rowType: .Email, strValue: updatedText, isValidation: isValidation)
            self.textField.leadingAssistiveLabel.text = updatedText.isEmpty ? "" : (self.isValidEmail(email: updatedText) ? "" : textFieldWithPodDataRec?.helperText)
            self.textField.setOutlineColor(updatedText.isEmpty ? .systemGray6 : (self.isValidEmail(email: updatedText) ? .green : .red), for: .normal)
            
        case .Password:
            // Checked textField has value or not
            // If Yes : Checked Password validation
            let isValidation = self.isValidPassword(password: updatedText)
            self.textFieldWithPodVM.setValue(rowType: .Password, strValue: updatedText, isValidation: isValidation)
            self.textField.leadingAssistiveLabel.text =  updatedText.isEmpty ? "" : (self.isValidPassword(password: updatedText) ? "" : textFieldWithPodDataRec?.helperText)
            self.textField.setOutlineColor(updatedText.isEmpty ? .systemGray6 : (self.isValidPassword(password: updatedText) ? .green : .red), for: .normal)
            
        case .ConfirmPassword:
            // Checked textField has value or not
            // If Yes : Checked Confirm Password  validation
            let password        = self.textFieldWithPodVM.getValue(rowType: .Password)
            let isValidation    = self.isValidConfirmPassword(password: password, confirmPassword: updatedText)
            self.textFieldWithPodVM.setValue(rowType: .ConfirmPassword, strValue: updatedText, isValidation: isValidation)
            self.textField.leadingAssistiveLabel.text = updatedText.isEmpty ? "" : (self.isValidConfirmPassword(password: password, confirmPassword: updatedText) ? "" : textFieldWithPodDataRec?.helperText)
    
            self.textField.setOutlineColor(updatedText.isEmpty ? .systemGray6 : (self.textFieldWithPodVM.getIsValidation(rowType: .ConfirmPassword) ? .green : .red), for: .normal)
            
        default:
            // Default for Username
            // Checked username validation
            let isValidation    = self.isValidUsername(username: updatedText)
            self.textFieldWithPodVM.setValue(rowType: .Username, strValue: updatedText, isValidation: isValidation)
            self.textField.leadingAssistiveLabel.text = updatedText.isEmpty ? "" : (self.isValidUsername(username: updatedText) ? "" : textFieldWithPodDataRec?.helperText)
            self.textField.setOutlineColor(updatedText.isEmpty ? .systemGray6 : (self.textFieldWithPodVM.getIsValidation(rowType: .Username) ? .green : .red), for: .normal)
        }
        // Set color to normal
        self.textField.setNormalLabelColor(UIColor(hexString: "A6A6A6"), for: .normal)
        textFieldWithPodDataRec?.value = textField.text?.trimmingCharacters(in: .whitespaces) ?? ""
        // Completion back to VC
        self.completionRec()
        return true
    }
      
    // Used for validation
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
         
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            
           // Get row type
            let rowType = self.textFieldWithPodDataRec?.rowType
             
            switch rowType {
            // Limit length for Password and Confirm Password
            case .Password, .ConfirmPassword:
                return updatedText.count < 15
                
            default:
                break
            }
        }
        return true
    }
}
