//
//  WithPodModel.swift
//  UITextFieldAndUITextView
//

import Foundation

struct TextFieldWithPodModel {
     
    var dataRec : [WithPodObject?]
    
    struct WithPodObject {
        var rowType         : RowType?
        var title           : String?
        var value           : String?
        var placeholder     : String?
        var helperText      : String?
        var isValidation    : Bool?
    }
    
}
