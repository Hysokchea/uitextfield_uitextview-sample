//
//  NormalTextFieldVM.swift
//  UITextField
//
//  Created by Hong Kheatbung on 8/1/23.
//

import Foundation

class NormalTextFieldVM {
    
    var normalTextFieldRec : [NormalTextFieldModel.NormalObject]?
    
    func initNormalTextField() {
        self.normalTextFieldRec = [
            NormalTextFieldModel.NormalObject(rowType       : .Username,
                                              title         : "Username",
                                              value         : "",
                                              placeholder   : "Enter username",
                                              isValidation  : false),
             
            NormalTextFieldModel.NormalObject(rowType       : .Email,
                                              title         : "Email",
                                              value         : "",
                                              placeholder   : "Enter email",
                                              isValidation  : false),
            
            NormalTextFieldModel.NormalObject(rowType       : .Password,
                                              title         : "Password",
                                              value         : "",
                                              placeholder   : "Enter password",
                                              isValidation  : false),
            
            NormalTextFieldModel.NormalObject(rowType       : .ConfirmPassword,
                                              title         : "Confirm Password",
                                              value         : "",
                                              placeholder   : "Enter confirm password",
                                              isValidation  : false),
        ]
    }
     
    /// Set value based on rowType
    func setValue(rowType: RowType, strValue: String, isValidation: Bool) {
        for (index, value) in (self.normalTextFieldRec?.enumerated())! {
            if value.rowType == rowType {
                self.normalTextFieldRec?[index].value           = strValue
                self.normalTextFieldRec?[index].isValidation    = isValidation
            }
        }
    }
    
    /// Get value based on rowType
    func getValue(rowType: RowType) -> String {
        var strValue = ""
        for (index, value) in (self.normalTextFieldRec?.enumerated())! {
            if value.rowType == rowType {
                strValue = self.normalTextFieldRec?[index].value ?? ""
            }
        }
        return strValue
    }
    
    /// Get row validation based on rowType
    func getIsValidation(rowType: RowType) -> Bool {
        var isValidation = false
        for (index, value) in (self.normalTextFieldRec?.enumerated())! {
            if value.rowType == rowType {
                isValidation = self.normalTextFieldRec?[index].isValidation ?? false
            }
        }
        return isValidation
    }
}
