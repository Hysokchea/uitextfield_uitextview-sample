//
//  NormalTextFieldCell.swift
//  UITextField
//
//  Created by Hong Kheatbung on 8/1/23.
//

import UIKit

class NormalTextFieldCell: UITableViewCell {

    @IBOutlet weak var lblTitle         : UILabel!
    @IBOutlet weak var textField        : UITextField!
    @IBOutlet weak var viewTextfield    : UIView!
    @IBOutlet weak var btnEye           : UIButton!
    
    var normalTextFieldVM   = NormalTextFieldVM()
    var normalTextFieldRec  : NormalTextFieldModel.NormalObject?
    var completionRec       : () -> Void = {}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Delegate textField
        self.textField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    // Config data to display
    func configCell(data: NormalTextFieldModel.NormalObject?) {
        self.lblTitle.text          = data?.title
        self.textField.placeholder  = data?.placeholder
        
        let rowType = data?.rowType
        switch rowType {
        case .Password, .ConfirmPassword:
            self.btnEye.isHidden = false    // Display btnEye
            
        default:
            self.btnEye.isHidden = true    // Hidden btnEye
        }
    }
    
    /// Limit length for username from 4-15
    private func isValidUsername(username: String) -> Bool {
        let RegEx       = "\\A\\w{4,15}\\z"
        let Test        = NSPredicate(format:"SELF MATCHES %@", RegEx)
        return Test.evaluate(with: username)
    }

    /// Email validation
    private func isValidEmail(email: String) -> Bool {
        let emailRegEx  = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred   = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }

    /// Password validation
    private func isValidPassword(password: String) -> Bool {
        let passwordRegex   = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*()\\-_=+{}|?>.<,:;~`’]{8,}$"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: password)
    }

    /// Comfirm password validation
    private func isValidConfirmPassword(password: String, confirmPassword: String) -> Bool {
        /// Checked: confirm password and password are matched
        let isPasswordMatch = password == confirmPassword
        let passwordRegex   = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*()\\-_=+{}|?>.<,:;~`’]{8,}$"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: confirmPassword) && isPasswordMatch
    }
     
    @IBAction func didTapBtnEye(_ sender: UIButton) {
        self.btnEye.isSelected              = !sender.isSelected
        self.textField.isSecureTextEntry    = !sender.isSelected
    }
    
}

// MARK: - UITextFieldDelegate
extension NormalTextFieldCell: UITextFieldDelegate {
    
    // Dismiss keyboard while user's click Return or Done key
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    // The text field in which editing is about to begin.
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        /// Set borderColor and borderWidth for view that contain textField
        self.viewTextfield.borderColor = .blue
        self.viewTextfield.borderWidth = 2
        
        let rowType = normalTextFieldRec?.rowType
        
        switch rowType {
        case .Email:
            /// Set keyboard to email keyboard
            textField.keyboardType = .emailAddress
            
        case .Password, .ConfirmPassword:
            /// Set secure text entry for Password and Confirm Password
            textField.isSecureTextEntry = !self.btnEye.isSelected
            
        /// Default for Username
        default:
            /// Set keyboard to defaul
            textField.keyboardType = .default
        }
        
        return true
    }

    // The text field in which editing is about to end.
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {

        /// Trimming WhiteSpace (" ")
        let updatedText = textField.text?.trimmingCharacters(in: .whitespaces) ?? ""
         
        /// Get row type
        let rowType = normalTextFieldRec?.rowType
        switch rowType {
        case .Email:
            /// Checked textField has value or not
            /// If Yes : Checked Email address validation
            let isValidation = self.isValidEmail(email: updatedText)
            self.normalTextFieldVM.setValue(rowType: .Email, strValue: updatedText, isValidation: isValidation)
            self.viewTextfield.borderColor = updatedText.isEmpty ? .systemGray6 : (self.normalTextFieldVM.getIsValidation(rowType: .Email) ? .green : .red)
            
        case .Password:
            /// Checked textField has value or not
            /// If Yes : Checked Password validation
            let isValidation = self.isValidPassword(password: updatedText)
            self.normalTextFieldVM.setValue(rowType: .Password, strValue: updatedText, isValidation: isValidation)
            self.viewTextfield.borderColor = updatedText.isEmpty ? .systemGray6 : (self.normalTextFieldVM.getIsValidation(rowType: .Password) ? .green : .red)
            
        case .ConfirmPassword:
            /// Checked textField has value or not
            /// If Yes : Checked Confirm Password  validation
            let password        = self.normalTextFieldVM.getValue(rowType: .Password)
            let isValidation    = self.isValidConfirmPassword(password: password, confirmPassword: updatedText)
            self.normalTextFieldVM.setValue(rowType: .ConfirmPassword, strValue: updatedText, isValidation: isValidation)
            self.viewTextfield.borderColor = updatedText.isEmpty ? .systemGray6 : (self.normalTextFieldVM.getIsValidation(rowType: .ConfirmPassword) ? .green : .red)
            
        default:
            /// Default for Username
            /// Checked textField has value or not
            let isValidation    = self.isValidUsername(username: updatedText)
            self.normalTextFieldVM.setValue(rowType: .Username, strValue: updatedText, isValidation: isValidation)
            self.viewTextfield.borderColor = updatedText.isEmpty ? .systemGray6 : (self.normalTextFieldVM.getIsValidation(rowType: .Username) ? .green : .red)
        }
         
        /// Set borderWidth to default
        self.viewTextfield.borderWidth = 1
        
        /// Completion back to VC
        self.completionRec()
         
        return true
    }

    // The text field calls this method whenever user actions cause its text to change.
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string) 
            
           /// Get row type
            let rowType = self.normalTextFieldRec?.rowType
            switch rowType {
            /// Limit length for Password and Confirm Password
            case .Password, .ConfirmPassword:
                return updatedText.count < 15
                
            default:
                break
            }
        }
        return true
    }
}
