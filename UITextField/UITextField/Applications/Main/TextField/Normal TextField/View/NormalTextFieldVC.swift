//
//  NormalTextFieldVC.swift
//  UITextField
//
//  Created by Hong Kheatbung on 8/1/23.
//

import UIKit

class NormalTextFieldVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnDone: UIButton!
    
    var normalTextFieldVM = NormalTextFieldVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Init data from VM
        self.normalTextFieldVM.initNormalTextField()
        
        // Set disable button to prevent any action
        self.btnDone.isEnabled = false
    }
    
    // Validation for btnDone
    private func doneBtnValidation() {
        /// Get Value from VM
        let isUsername          = !self.normalTextFieldVM.getValue(rowType: .Username).isEmpty          && self.normalTextFieldVM.getIsValidation(rowType: .Username)
        let isEmail             = !self.normalTextFieldVM.getValue(rowType: .Email).isEmpty             && self.normalTextFieldVM.getIsValidation(rowType: .Email)
        let isPassword          = !self.normalTextFieldVM.getValue(rowType: .Password).isEmpty          && self.normalTextFieldVM.getIsValidation(rowType: .Password)
        let isConfirmPassword   = !self.normalTextFieldVM.getValue(rowType: .ConfirmPassword).isEmpty   && self.normalTextFieldVM.getIsValidation(rowType: .ConfirmPassword)
        
        let isEnable            = isUsername && isEmail && isPassword && isConfirmPassword
        
        self.btnDone.isEnabled          = isEnable
        self.btnDone.backgroundColor    = isEnable ? .blue : .lightGray
        self.btnDone.setTitleColor(isEnable ? .cyan : .darkGray, for: .normal)
    }
    
    @IBAction func didTapBtnDone(_ sender: UIButton) {
        let username        = self.normalTextFieldVM.getValue(rowType: .Username)
        let email           = self.normalTextFieldVM.getValue(rowType: .Email)
        let password        = self.normalTextFieldVM.getValue(rowType: .Password)
        let confirmPassword = self.normalTextFieldVM.getValue(rowType: .ConfirmPassword)
          
        self.customAlert(type       : .CONFIRM_ALERT,
                         image      : "check",
                         title      : "Your Information",
                         message    : "User name: \(username) \n\nEmail: \(email) \n\nPassword: \(password) \n\nConfirm Password: \(confirmPassword)",
                         greyTitle: "Confirm") { (isRedPress) in
            /// Back to root
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
}

// MARK: - UITableViewDelegate
extension NormalTextFieldVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.normalTextFieldVM.normalTextFieldRec?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NormalTextFieldCell") as! NormalTextFieldCell
        cell.normalTextFieldVM  = self.normalTextFieldVM
        cell.normalTextFieldRec = self.normalTextFieldVM.normalTextFieldRec?[indexPath.row]
        cell.configCell(data: self.normalTextFieldVM.normalTextFieldRec?[indexPath.row])
        
        /// Completion from NormalTextFieldCell
        cell.completionRec = {
            
            /// Validation for btnDone
            self.doneBtnValidation()
        }
        
        return cell
    }
}
