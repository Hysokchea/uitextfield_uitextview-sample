//
//  NormalTextFieldModel.swift
//  UITextField
//
//  Created by Hong Kheatbung on 8/1/23.
//

import Foundation

enum RowType {
    case Username
    case Email
    case Password
    case ConfirmPassword
}

struct NormalTextFieldModel {
     
    var object : [NormalObject?]
    
    struct NormalObject {
        var rowType         : RowType?
        var title           : String?
        var value           : String?
        var placeholder     : String?
        var isValidation    : Bool?
    }
    
}
