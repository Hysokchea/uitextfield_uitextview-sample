//
//  NormalTextViewVC.swift
//  UITextField
//
//  Created by Hong Kheatbung on 8/1/23.
//

import UIKit

class NormalTextViewVC: UIViewController {

    @IBOutlet weak var tableView    : UITableView!
    @IBOutlet weak var btnDone      : UIButton!
    
    var normalTextViewVM = NormalTextViewVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Init data from VM
        self.normalTextViewVM.initNormalTextView()
        
        // Set disable button to prevent any action
        self.btnDone.isEnabled = false
    }
    
    /// Validation for btnDone
    private func doneBtnValidation() {
        let isUsername          = !self.normalTextViewVM.getValue(rowType: .Username).isEmpty           && self.normalTextViewVM.getIsValidation(rowType: .Username)
        let isEmail             = !self.normalTextViewVM.getValue(rowType: .Email).isEmpty              && self.normalTextViewVM.getIsValidation(rowType: .Email)
        let isPassword          = !self.normalTextViewVM.getValue(rowType: .Password).isEmpty           && self.normalTextViewVM.getIsValidation(rowType: .Password)
        let isConfirmPassword   = !self.normalTextViewVM.getValue(rowType: .ConfirmPassword).isEmpty    && self.normalTextViewVM.getIsValidation(rowType: .ConfirmPassword)
        
        let isEnable            = isUsername && isEmail && isPassword && isConfirmPassword
        
        self.btnDone.isEnabled          = isEnable
        self.btnDone.backgroundColor    = isEnable ? .blue : .lightGray
        self.btnDone.setTitleColor(isEnable ? .cyan : .darkGray, for: .normal)
    }
    
    @IBAction func didTapBtnDone(_ sender: UIButton) {
        let username        = self.normalTextViewVM.getValue(rowType: .Username)
        let email           = self.normalTextViewVM.getValue(rowType: .Email)
        let password        = self.normalTextViewVM.getValue(rowType: .Password)
        let confirmPassword = self.normalTextViewVM.getValue(rowType: .ConfirmPassword)
          
        self.customAlert(type       : .CONFIRM_ALERT,
                         image      : "check",
                         title      : "Your Information",
                         message    : "User name: \(username) \n\nEmail: \(email) \n\nPassword: \(password) \n\nConfirm Password: \(confirmPassword)",
                         greyTitle: "Confirm") { (isRedPress) in
            /// Back to root
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
}

// MARK: - UITableViewDelegate
extension NormalTextViewVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.normalTextViewVM.normalTextViewRec?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NormalTextViewCell") as! NormalTextViewCell 
        cell.normalTextViewVM  = self.normalTextViewVM
        cell.normalTextViewRec = self.normalTextViewVM.normalTextViewRec?[indexPath.row]
        cell.configCell(data: self.normalTextViewVM.normalTextViewRec?[indexPath.row])
        
        /// Completion from NormalTextFieldCell
        cell.completionRec = {
            
            // Validation for btnDone
            self.doneBtnValidation()
        }
        
        return cell
    }
}
