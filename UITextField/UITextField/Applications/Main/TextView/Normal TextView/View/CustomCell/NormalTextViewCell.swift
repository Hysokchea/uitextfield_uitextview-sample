//
//  NormalTextViewCell.swift
//  UITextFieldAndUITextView
//

import UIKit

class NormalTextViewCell: UITableViewCell {
       
    @IBOutlet weak var lblTitle     : UILabel!

    @IBOutlet weak var textView     : UITextView!
    
    @IBOutlet weak var viewTextView : UIView!
    
    var normalTextViewVM    = NormalTextViewVM()
    var normalTextViewRec   : NormalTextViewModel.NormalObject?
    var completionRec       : () -> Void = {}
    
    var strPassword         : String = ""
    var strConfirmPassword  : String = ""

    override func awakeFromNib() {
        super.awakeFromNib()
        // Delegate textView
        self.textView.delegate  = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
     
    // Config data to display
    func configCell(data: NormalTextViewModel.NormalObject?) {
        self.lblTitle.text      = normalTextViewRec?.title
        self.textView.text      = (normalTextViewRec?.value?.isEmpty ?? true) ? data?.placeholder : data?.value
        self.textView.textColor = (normalTextViewRec?.value?.isEmpty ?? true) ? .lightGray : .black // Which color decide to display
    }
    
    // Limit length for username from 4-15
    private func isValidUsername(username: String) -> Bool {
        let RegEx       = "\\A\\w{4,15}\\z"
        let Test        = NSPredicate(format:"SELF MATCHES %@", RegEx)
        return Test.evaluate(with: username)
    }
     
    // Email validation
    private func isValidEmail(email: String) -> Bool {
        let emailRegEx  = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred   = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    // Password validation
    func isValidPassword(password: String) -> Bool {
        let passwordRegex   = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*()\\-_=+{}|?>.<,:;~`’]{8,}$"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: password)
    }
    
    // Comfirm password validation
    private func isValidConfirmPassword(password: String, confirmPassword: String) -> Bool {
        // Checked: confirm password and password are matched
        let isPasswordMatch = password == confirmPassword
        let passwordRegex   = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*()\\-_=+{}|?>.<,:;~`’]{8,}$"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: confirmPassword) && isPasswordMatch
    }
}

// MARK: - UITextViewDelegate
extension NormalTextViewCell: UITextViewDelegate {
     
    //Asks the delegate whether to begin editing in the specified text view.
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        viewTextView.borderWidth    = 2
        // Set borderColor for focusing user attention
        viewTextView.borderColor    = .blue
        
        // Checked normalDataRec?.value has value or not to be display like textfield
        textView.text = (normalTextViewRec?.value?.isEmpty ?? true) ? nil : normalTextViewRec?.value
        
        let rowType = normalTextViewRec?.rowType
        
        switch rowType {
        case .Email:
            // Set keyboard to email keyboard
            textView.keyboardType = .emailAddress
              
        // Default for Username
        default:
            // Set keyboard to defaul
            textView.keyboardType = .default
        }
        return true
    }
      
    //Asks the delegate whether to stop editing in the specified text view
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        // Trimming WhiteSpace (" ")
        let updatedText = textView.text?.trimmingCharacters(in: .whitespaces) ?? ""
        let rowType = normalTextViewRec?.rowType
        switch rowType {
        case .Email:
            // Checked textField has value or not
            // If Yes : Checked Email address validation
            let isValidation = self.isValidEmail(email: updatedText)
            self.normalTextViewVM.setValue(rowType: .Email, strValue: updatedText, isValidation: isValidation)
            self.viewTextView.borderColor = updatedText.isEmpty ? .systemGray6 : (self.normalTextViewVM.getIsValidation(rowType: .Email) ? .green : .red)
        case .Password:
            // Checked textField has value or not
            // If Yes : Checked Password validation
            let isValidation = self.isValidPassword(password: updatedText)
            self.normalTextViewVM.setValue(rowType: .Password, strValue: updatedText, isValidation: isValidation)
            self.viewTextView.borderColor = updatedText.isEmpty ? .systemGray6 : (self.normalTextViewVM.getIsValidation(rowType: .Password) ? .green : .red)
        case .ConfirmPassword:
            // Checked textField has value or not
            // If Yes : Checked Confirm Password  validation
            let password        = self.normalTextViewVM.getValue(rowType: .Password)
            let isValidation    = self.isValidConfirmPassword(password: password, confirmPassword: updatedText)
            self.normalTextViewVM.setValue(rowType: .ConfirmPassword, strValue: updatedText, isValidation: isValidation)
            self.viewTextView.borderColor = updatedText.isEmpty ? .systemGray6 : (self.normalTextViewVM.getIsValidation(rowType: .ConfirmPassword) ? .green : .red)
        default:
            // Default for Username
            // Checked textField has value or not
            let isValidation    = self.isValidUsername(username: updatedText)
            self.normalTextViewVM.setValue(rowType: .Username, strValue: updatedText, isValidation: isValidation)
            self.viewTextView.borderColor = updatedText.isEmpty ? .systemGray6 : (self.normalTextViewVM.getIsValidation(rowType: .Username) ? .green : .red)
        }
        self.textView.text      = (self.normalTextViewRec?.value?.isEmpty ?? true) ? self.normalTextViewRec?.placeholder : self.normalTextViewRec?.value
        self.textView.textColor = (self.normalTextViewRec?.value?.isEmpty ?? true) ? .lightGray : .black
        // Set borderWidth to default
        self.viewTextView.borderWidth = 1
        // Completion back to VC
        self.completionRec()
        return true
    }
    
    //Tells the delegate when editing of the specified text view begins.
    func textViewDidChange(_ textView: UITextView) {
        // Trimming WhiteSpace (" ")
        let updatedText                 = textView.text?.trimmingCharacters(in: .whitespaces) ?? ""
        self.normalTextViewRec?.value   = updatedText
        self.textView.textColor         = (self.normalTextViewRec?.value?.isEmpty ?? true) ? .lightGray : .black
    }
}
