//
//  NormalTextViewVM.swift
//  UITextField
//
//  Created by Hong Kheatbung on 8/1/23.
//

import Foundation

class NormalTextViewVM {
    
    var normalTextViewRec : [NormalTextViewModel.NormalObject]?
    
    func initNormalTextView() {
        self.normalTextViewRec = [
            NormalTextViewModel.NormalObject(rowType       : .Username,
                                              title         : "Username",
                                              value         : "",
                                              placeholder   : "Enter username",
                                              isValidation  : false),
             
            NormalTextViewModel.NormalObject(rowType       : .Email,
                                              title         : "Email",
                                              value         : "",
                                              placeholder   : "Enter email",
                                              isValidation  : false),
            
            NormalTextViewModel.NormalObject(rowType       : .Password,
                                              title         : "Password",
                                              value         : "",
                                              placeholder   : "Enter password",
                                              isValidation  : false),
            
            NormalTextViewModel.NormalObject(rowType       : .ConfirmPassword,
                                              title         : "Confirm Password",
                                              value         : "",
                                              placeholder   : "Enter confirm password",
                                              isValidation  : false),
        ]
    }
     
    /// Set value based on rowType
    func setValue(rowType: RowType, strValue: String, isValidation: Bool) {
        for (index, value) in (self.normalTextViewRec?.enumerated())! {
            if value.rowType == rowType {
                self.normalTextViewRec?[index].value           = strValue
                self.normalTextViewRec?[index].isValidation    = isValidation
            }
        }
    }
    
    /// Get value based on rowType
    func getValue(rowType: RowType) -> String {
        var strValue = ""
        for (index, value) in (self.normalTextViewRec?.enumerated())! {
            if value.rowType == rowType {
                strValue = self.normalTextViewRec?[index].value ?? ""
            }
        }
        return strValue
    }
    
    /// Get row validation based on rowType
    func getIsValidation(rowType: RowType) -> Bool {
        var isValidation = false
        for (index, value) in (self.normalTextViewRec?.enumerated())! {
            if value.rowType == rowType {
                isValidation = self.normalTextViewRec?[index].isValidation ?? false
            }
        }
        return isValidation
    }
}
